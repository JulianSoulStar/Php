<?php
//$ _SERVER es una variable súper mundial PHP que contiene información sobre los encabezados,
// rutas y ubicaciones de scripts.
//
//El siguiente ejemplo muestra cómo utilizar algunos de los elementos en $ _SERVER:
echo $_SERVER['PHP_SELF'];
echo "<br>";
echo $_SERVER['SERVER_NAME'];
echo "<br>";
echo $_SERVER['HTTP_HOST'];
echo "<br>";
echo $_SERVER['HTTP_REFERER'];
echo "<br>";
echo $_SERVER['HTTP_USER_AGENT'];
echo "<br>";
echo $_SERVER['SCRIPT_NAME'];

//PHP $ _REQUEST se utiliza para recoger datos después de la presentación de un formulario HTML.
//
//El siguiente ejemplo muestra un formulario con un campo de entrada y un botón de envío.
// Cuando un usuario envía los datos haciendo clic en "Enviar",
// los datos del formulario se envía al archivo especificado en el atributo action de la etiqueta
// <form>. En este ejemplo, nosotros señalamos el propio archivo de datos de formularios de
// procesamiento. Si desea utilizar otro archivo PHP para procesar los datos del formulario,
// reemplace que con el nombre de su elección. Entonces,
// podemos utilizar el súper variable global $ _REQUEST para recoger el valor del campo
// de entrada:
//
//<html>
//<body>
//
//<form method="post" action="<?php echo $_SERVER['PHP_SELF'];
?>">
//  Name: <input type="text" name="fname">
//  <input type="submit">
//</form>
//
//<?php
//if ($_SERVER["REQUEST_METHOD"] == "POST") {
//    // collect value of input field
//    $name = $_REQUEST['fname'];
//    if (empty($name)) {
//        echo "Name is empty";
//    } else {
//        echo $name;
//    }
//}
//
//</body>
//</html>
//PHP $ _POST es ampliamente utilizado para recoger datos del formulario después de enviar un formulario HTML con method = "post". $ _POST también se usa ampliamente para pasar variables.
//El siguiente ejemplo muestra un formulario con un campo de entrada y un botón de envío.
// Cuando un usuario envía los datos haciendo clic en "Enviar",
// los datos del formulario se envía al archivo especificado en el atributo action de la etiqueta
// <form>. En este ejemplo, señalamos que el propio archivo de datos de formularios de procesamiento.
// Si desea utilizar otro archivo PHP para procesar los datos del formulario,
// reemplace que con el nombre de su elección. Entonces, podemos utilizar el súper variable global
// $ _POST para recoger el valor del campo de entrada:
//<html>
//<body>
//
//<form method="post" action="<?php echo $_SERVER['PHP_SELF'];
?>">
//  Name: <input type="text" name="fname">
//  <input type="submit">
//</form>
//
//<?php
//if ($_SERVER["REQUEST_METHOD"] == "POST") {
//    // collect value of input field
//    $name = $_POST['fname'];
//    if (empty($name)) {
//        echo "Name is empty";
//    } else {
//        echo $name;
//    }
//}
//
//</body>
//</html>
//PHP $ _GET también se puede utilizar para recoger datos del formulario después de enviar un formulario HTML con method = "get".
//$ _GET también puede recoger datos enviados en la URL.
//
//Supongamos que tenemos una página HTML que contiene un hipervínculo con parámetros:
//<html>
//<body>
//
//<a href="test_get.php?subject=PHP&web=W3schools.com">Test $GET</a>
//
//</body>
//</html>
//Una variable tipo array asociativo de variables pasadas al script actual a través de Cookies HTTP.
//$HTTP_COOKIE_VARS contiene la misma información inicial,
// pero no es una superglobal. (Note que $HTTP_COOKIE_VARS y $_COOKIE son variables diferentes
//  y que PHP las trata como tal)
//ejemplo:
echo '¡Hola ' . htmlspecialchars($_COOKIE["nombre"]) . '!';
//resultado : ¡Hola Juan!