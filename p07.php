<?php

$texto = "Hola mundo";
//
//si escribimos el nombre de una variable dentro de una cadena de texto,
//no veremos el nombre de la variable, sino su valor. por tanto si escribimos ,
// siguiendo con el ejemplo anterior, el texto
echo "Bienvenido a mi página, $texto";

//nos dará el siguiente resultado en la página:

//Bienvenido a mi página, Hola mundo
