<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>
  <body>
    <!--La instrucción echo es una de las más utilizadas en PHP,
    ya que nos permite visualizar en pantalla lo que escribamos detrás de esta palabra clave.
    Si lo que queremos visualizar es un texto, debemos escribirlo entre comillas.
    de la siguiente manera:-->
    <?php
    echo "Hola mundo";
    ?>

    <!--// Cuando PHP analiza un fichero, busca las etiquetas de apertura y cierre, que son <?php y
    ?>,
        y que indican a PHP dónde empezar y finalizar la interpretación del código.
        Este mecanismo permite embeber a PHP en todo tipo de documentos,
        ya que todo lo que esté fuera de las etiquetas de apertura
        y cierre de PHP será ignorado por el analizador.-->
  </body>
</html>
<!--    //También podemos indicar que estamos escribiendo en PHP mediante la etiqueta:
<script language="php">//.... codigo php ... </script>
//Hay otras etiquetas para delimitar el código PHP,
//pero no funcionan bien con todas las versiones de "Apache" o de "XAMPP". estas son:
//<? .... codigo php ...
    ?>
<% .... codigo php ... %>-->

