<?php

//En PHP todas las variables van precedidas del signo dolar $.
//este signo delante de cualquier texto indica que lo que viene detrás es una variable:
// ejemplo: $miVariable
$texto = "Hola mundo";
//
//si escribimos el nombre de una variable dentro de una cadena de texto,
//no veremos el nombre de la variable, sino su valor. por tanto si escribimos ,
// siguiendo con el ejemplo anterior, el texto
echo "Bienvenido a mi página, $texto";

//nos dará el siguiente resultado en la página:

//Bienvenido a mi página, Hola mundo
