<?php

//Array :
//Un array almacena varios valores en una sola variable:
//Un array es una variable especial, que puede contener más de un valor a la vez.

$cars = array("ferrari", "BMW", "Toyota");
echo "I like " . $cars[0] . ", " . $cars[1] . " and " . $cars[2] . ".";
