<?php

//En programación orientada a objetos el principio de ocultación hace referencia a
// que los atributos privados de un objeto no pueden ser modificados ni obtenidos a no ser que se haga
// a través del paso de un mensaje (invocación a métodos, ya sean estos funciones o procedimientos).