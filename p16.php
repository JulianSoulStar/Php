<?php

//sort () - ordenar matrices de clasificación en orden ascendente
//rsort () - ordenar matrices de clasificación en orden descendente
//asort () - ordenar matrices asociativas en orden ascendente, de acuerdo con el valor
//ksort () - ordenar matrices asociativas en orden ascendente, de acuerdo con la clave
//arsort () - ordenar matrices asociativas en orden descendente, de acuerdo con el valor
//krsort () - ordenar matrices asociativas en orden descendente, de acuerdo con la clave
//
//El siguiente ejemplo ordena los elementos de la matriz $ coches en orden alfabético ascendente:
//sort:
$cars = array("Volvo", "BMW", "Toyota");
sort($cars);

//El siguiente ejemplo ordena los elementos de la matriz $ coches en orden alfabético descendente:
//rsort:
$cars2 = array("Volvo", "BMW", "Toyota");
rsort($cars2);

//El siguiente ejemplo ordena una matriz asociativa en orden ascendente, de acuerdo con el valor:
//asort:
$age = array("Peter" => "35", "Ben" => "37", "Joe" => "43");
asort($age);

//El siguiente ejemplo ordena una matriz asociativa en orden ascendente, de acuerdo con la clave:
//ksort:
$age2 = array("Peter" => "35", "Ben" => "37", "Joe" => "43");
ksort($age2);

//El siguiente ejemplo ordena una matriz asociativa en orden descendente, de acuerdo con el valor:
//arsort:
$age3 = array("Peter" => "35", "Ben" => "37", "Joe" => "43");
arsort($age3);

//El siguiente ejemplo ordena una matriz asociativa en orden descendente, de acuerdo con la clave:
//krsort :
$age4 = array("Peter" => "35", "Ben" => "37", "Joe" => "43");
krsort($age4);
