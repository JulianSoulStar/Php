<?php

//Las funciones anónimas, también conocidas como cierres (closures),
//permiten la creación de funciones que no tienen un nombre especificado.
//Son más útiles como valor de los parámetros de llamadas de retorno, pero tienen muchos otros usos.
//
//Las funciones anónimas están implementadas utilizando la clase Closure.
echo preg_replace_callback('~-([a-z])~', function ($coincidencia) {
  return strtoupper($coincidencia[1]);
}, 'hola-mundo');
// imprime holaMundo