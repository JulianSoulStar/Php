<?php

//La visibilidad de una propiedad, un método o una constante se puede definir
// anteponiendo a su declaración una de las palabras reservadas public, protected o private.
// A los miembros de clase declarados como 'public' se puede acceder desde donde sea;
// a los miembros declarados como 'protected', solo desde la misma clase o mediante clases heredadas.
// A los miembros declarados como 'private' únicamente se puede acceder desde la clase que los definió.
//Las propiedades de clases deben ser definidas como 'public', 'private' o 'protected'.
// Si se declaran usando var, serán definidas como 'public'.
// ejemplo :
/**
 * Definición de MyClass
 */
class MyClass {

  public $public = 'Public';
  protected $protected = 'Protected';
  private $private = 'Private';

  function printHello() {
    echo $this->public;
    echo $this->protected;
    echo $this->private;
  }

}

$obj = new MyClass();
echo $obj->public;    // Funciona bien
echo $obj->protected; // Error Fatal
echo $obj->private;   // Error Fatal
$obj->printHello();   // Muestra Public, Protected y Private

/**
 * Definición de MyClass2
 */
class MyClass2 extends MyClass {

  // Se pueden redeclarar las propiedades pública y protegida, pero no la privada
  public $public = 'Public2';
  protected $protected = 'Protected2';

  function printHello() {
    echo $this->public;
    echo $this->protected;
    echo $this->private;
  }

}

$obj2 = new MyClass2();
echo $obj2->public;    // Funciona bien
echo $obj2->protected; // Error Fatal
echo $obj2->private;   // Undefined
$obj2->printHello();   // Muestra Public2, Protected2, Undefined
//
//PHP 5 introduce clases y métodos abstractos. Las clases definidas como abstractas
// no se pueden instanciar y cualquier clase que contiene al menos un método abstracto
// debe ser definida como tal. Los métodos definidos como abstractos simplemente declaran
// la firma del método, pero no pueden definir la implementación.
// ejemplo clase abstracta :

abstract class ClaseAbstracta {

  // Forzar la extensión de clase para definir este método
  abstract protected function getValor();

  abstract protected function valorPrefijo($prefijo);

  // Método común
  public function imprimir() {
    print $this->getValor() . "\n";
  }

}

class ClaseConcreta1 extends ClaseAbstracta {

  protected function getValor() {
    return "ClaseConcreta1";
  }

  public function valorPrefijo($prefijo) {
    return "{$prefijo}ClaseConcreta1";
  }

}

class ClaseConcreta2 extends ClaseAbstracta {

  public function getValor() {
    return "ClaseConcreta2";
  }

  public function valorPrefijo($prefijo) {
    return "{$prefijo}ClaseConcreta2";
  }

}

$clase1 = new ClaseConcreta1;
$clase1->imprimir();
echo $clase1->valorPrefijo('FOO_') . "\n";

$clase2 = new ClaseConcreta2;
$clase2->imprimir();
echo $clase2->valorPrefijo('FOO_') . "\n";

//Las interfaces de objetos permiten crear código con el cual especificar qué métodos deben ser implementados por una clase, sin tener que definir cómo estos métodos son manipulados.
//Las interfaces se definen de la misma manera que una clase, aunque reemplazando
// la palabra reservada class por la palabra reservada interface y sin que ninguno de sus métodos
// tenga su contenido definido.
//
//Todos los métodos declarados en una interfaz deben ser públicos,
// ya que ésta es la naturaleza de una interfaz.
//ejemplo de interfaz :
// Declarar la interfaz 'iTemplate'
interface iTemplate {

  public function setVariable($name, $var);

  public function getHtml($template);
}

// Implementar la interfaz
// Ésto funcionará
class Template implements iTemplate {

  private $vars = array();

  public function setVariable($name, $var) {
    $this->vars[$name] = $var;
  }

  public function getHtml($template) {
    foreach ($this->vars as $name => $value) {
      $template = str_replace('{' . $name . '}', $value, $template);
    }

    return $template;
  }

}

// Ésto no funcionará
// Error fatal: La Clase BadTemplate contiene un método abstracto
// y por lo tanto debe declararse como abstracta (iTemplate::getHtml)
class BadTemplate implements iTemplate {

  private $vars = array();

  public function setVariable($name, $var) {
    $this->vars[$name] = $var;
  }

}
