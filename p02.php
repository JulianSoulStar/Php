
//En PHP el tipo de datos de una variable no está definido por el programador.
//PHP decide el tipo de datos de las variables después de interpretar la página web.
//El tipo de datos básicos incluidos en php son variables,
// que no son más que identificadores para la ubicación de memoria para almacenar datos.
//
// Tipo de dato	 Significado
//
//boolean	Puede ser True o False
//entero	Un numero entero
//flotante	Un número que puede tener un decimal
//Cadena	Una serie de caracteres
//Matriz	Contiene nombres asignados a valores
//Objeto	Un tipo que puede contener propiedades y métodos
//Recurso	Contiene una referencia a un recurso externo, como por ejemplo un controlador a un archivo abierto
//NULL	La variable no contiene ningún valor
//
//Booleanos:

//Para hacer una variable de este tipo tan solo hay que escribir su nombre y asignarle o true o false; ya que éstos son los dos únicos datos booleanos que existen.
//
<?php
$guapo = true;
$simpatico = false;

// con el resto es igual que JavaScript

