<?php
//La sentencia if se ejecuta algún código si una condición es verdadera.
$t = 10;

if ($t < "20") {
  echo "Have a good day!";
}

//La sentencia else if .... Ejecuta código si una condición es verdadera y otro código si la condición es falsa.
$a = 21;

if ($a < "20") {
  echo "Have a good day!";
} else {
  echo "Have a good night!";
}

//El caso .... elseif ... Else ejecuta códigos diferentes por más de dos condiciones.
$z = 5;

if ($z < "10") {
  echo "Have a good morning!";
} elseif ($z < "20") {
  echo "Have a good day!";
} else {
  echo "Have a good night!";
}

//While - a través de bucles de un bloque de código, siempre y cuando la condición especificada es verdadera
$x = 1;

while ($x <= 5) {
  echo "The number is: $x <br>";
  $x++;
}
//do ... while - bucles a través de un bloque de código una vez, y luego repite el bucle mientras la condición especificada es verdadera
$b = 1;

do {
  echo "The number is: $b <br>";
  $b++;
} while ($b <= 5);
//For - bucles a través de un bloque de código un número determinado de veces
for ($y = 0; $y <= 10; $y++) {
  echo "The number is: $y <br>";
}
//Foreach - bucle a través de un bloque de código para cada elemento de una matriz
$colors = array("red", "green", "blue", "yellow");

foreach ($colors as $value) {
  echo "$value <br>";
}

//Utilice la sentencia switch para seleccionar uno de los muchos bloques de código para ser ejecutado .
//Así es como funciona:
//En primer lugar tenemos una sola expresión n (con mayor frecuencia una variable),que se evalúa una vez.
//El valor de la expresión se compara entonces con los valores para cada caso en la estructura.
//Si hay una coincidencia, se ejecuta el bloque de código asociado a ese caso.
//Utilice BREAK para evitar que el código se ejecute en el siguiente caso automáticamente.
//El valor predeterminado instrucción se utiliza si no se encuentra ninguna coincidencia.
$favcolor = "red";

switch ($favcolor) {
  case "red":
    echo "Your favorite color is red!";
    break;
  case "blue":
    echo "Your favorite color is blue!";
    break;
  case "green":
    echo "Your favorite color is green!";
    break;
  default:
    echo "Your favorite color is neither red, blue, nor green!";
}
//break finaliza la ejecución de la estructura for, foreach, while, do-while o switch en curso.
//
//continue se utiliza dentro de las estructuras iterativas para saltar el resto de la iteración actual
// del bucle y continuar la ejecución en la evaluación de la condición,
// para luego comenzar la siguiente iteración.
//return devuelve el control del programa al módulo que lo invoca.
//La ejecución vuelve a la siguiente expresión después del módulo que lo invoca.
//Si se llama desde una función, la sentencia return inmediatamente termina la ejecución de la función
// actual, y devuelve su argumento como el valor de la llamada a la función.
//  return también termina la ejecución de una sentencia eval() o un archivo de script.
//a.php
include("b.php");
echo "a";
?>

b.php
<?php
echo "b";
return;
//require es idéntico a include excepto que en caso de fallo producirá un error fatal de nivel
// E_COMPILE_ERROR. En otras palabras,
// éste detiene el script mientras que include sólo emitirá una advertencia (E_WARNING)
// lo cual permite continuar el script.
//
//La sentencia include incluye y evalúa el archivo especificado.
//
//La sentencia require_once es idéntica a require excepto que PHP verificará si el archivo ya ha sido
// incluido y si es así, no se incluye (require) de nuevo.
// 
//La sentencia include_once incluye y evalúa el fichero especificado durante la ejecución del script.
// Tiene un comportamiento similar al de la sentencia include,
// siendo la única diferencia de que si el código del fichero ya ha sido incluido,
// no se volverá a incluir, e include_once devolverá TRUE. Como su nombre indica,
// el fichero será incluido solamente una vez.
include_once "a.php"; // esto incluirá a.php
include_once "A.php"; // esto incluirá a.php ¡otra vez! (sólo PHP 4)