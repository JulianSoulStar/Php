<?php

//En bastantes servidores no se muestran los errores PHP por defecto,
// de ahí que muchas veces al no obtener el resultado o funcionamiento esperado nos rompemos
// la cabeza revisando líneas y líneas de PHP ignorando que verdaderamente el script nos está dando error.
//
//Con las siguientes funciones colocadas en el comienzo de nuestros ficheros PHP haremos que los
// errores PHP salgan a la luz:
error_reporting(E_ALL);
ini_set('display_errors', '1');

//mas codigos de ejemplo
rror_reporting(-1);

// No mostrar los errores de PHP
error_reporting(0);

// Motrar todos los errores de PHP
error_reporting(E_ALL);

// Motrar todos los errores de PHP
ini_set('error_reporting', E_ALL);
