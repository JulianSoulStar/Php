<?php

//Tipos de variables
//Las variables pueden ser de diferentes tipos en función del valor que almacenan. los tipos de variables son:
//Números enteros (integer)
//Números enteros sin decimales. Ejemplo:
$num1 = 43;
//Números Reales (real)
//Números reales, con decimales. Ejemplo:
$num2 = 12.56;
//Cadenas de texto (string)
//Texto escrito con caracteres alfamuméricos. Van siempre delimitadas por comillas al principio y al final de la cadena. Ejemplo:
$texto = "Esta variable es un texto.";
//Arrays o listas de elementos.
//un array es un conjunto de elementos que están agrupados bajo una única variable, y forman una lista. Si conoces el lenguaje Javascript ya sabrás lo que son los arrays. La forma de trabajar con los arrays en PHP es similar a Javascript, aunque no es idéntica. Veremos más adelante como trabajar con arrays en PHP. Aunque más adelante se explicará con más detalle, este es un ejemplo de como crear un Array
$array = array("primavera", "verano", "otoño", "invierno");
