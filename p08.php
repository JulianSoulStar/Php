<?php

$text = "concatenando esto";
$name = "Julian";
//
//si escribimos el nombre de una variable dentro de una cadena de texto,
//no veremos el nombre de la variable, sino su valor. por tanto si escribimos ,
// siguiendo con el ejemplo anterior, el texto
echo "Hola soy $name y estoy $texto";

//nos dará el siguiente resultado en la página:

//Hola soy Julian y estoy concatenando esto
