<?php

// encapsulacion
//En programación modular, y más específicamente en programación orientada a objetos,
// se denomina encapsulamiento al ocultamiento del estado, es decir,
// de los datos miembro de un objeto de manera que solo se pueda cambiar mediante
// las operaciones definidas para ese objeto.
//
//Cada objeto está aislado del exterior, es un módulo natural,
//y la aplicación entera se reduce a un agregado o rompecabezas de objetos.
//El aislamiento protege a los datos asociados de un objeto contra su modificación
//por quien no tenga derecho a acceder a ellos, eliminando efectos secundarios e interacciones.
//
//De esta forma el usuario de la clase puede obviar la implementación de los métodos
//y propiedades para concentrarse solo en cómo usarlos. Por otro lado se evita que el usuario
//pueda cambiar su estado de maneras imprevistas e incontroladas.

