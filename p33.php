<?php

//La modularidad es, en programación modular y más específicamente en programación orientada a objetos,
// la propiedad que permite subdividir una aplicación en partes más pequeñas (llamadas módulos),
//cada una de las cuales debe ser tan independiente como sea posible de la aplicación en sí
//y de las restantes partes.
//
//Estos módulos que se puedan compilar por separado, pero que tienen conexiones con otros módulos.
// Al igual que la encapsulación, los lenguajes soportan la Modularidad de diversas formas.
// La modularidad debe seguir los conceptos de acoplamiento y cohesión.