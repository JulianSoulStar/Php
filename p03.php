<?php

// Comentarios
//
//Su sintaxis es la misma que en javascript. Tenemos dos tipos de comentarios: los de una línea y los de varias líneas.
//Los comentarios de una línea empiezan con dos barras inclinadas:
//. Todo lo que se ponga en la línea después de las dos barras inclinadas será el comentario:
$a = "hola"; //definir la variable a
//Después de la instrucción hemos puesto un comentario de una línea para indicar lo que estamos haciendo.
//Los comentarios de varias líneas empiezan por /* y terminan por */
// todo lo que escribamos entre estos signos será un comentario:

/*página de inicio
Creada por Anyelguti.*/