<?php

/* Constantes
 * Las constantes son como las variables,
 * excepto que una vez que se definen no pueden ser cambiados o indefinido.
 * Una constante es un identificador (nombre) para un valor simple. El valor no se puede cambiar durante la secuencia de comandos.

  Un nombre válido constante comienza con una letra o un guión bajo
 *  (sin signo $ antes del nombre de constante).

  Nota: A diferencia de las variables,
 * las constantes son automáticamente mundial a través de toda la secuencia de comandos.
 * Para crear una constante, utilizar la función de definir ().
 *
 * define(name, value, case-insensitive)
 *
 * name : especifica el nombre de la constante
  value : Especifica el valor de la constante
  mayúsculas y minúsculas : especifica si el nombre de la constante debería ser sensible a las mayúsculas.
 * Predeterminado es falso
 */
define("text", "Hello World");
echo text;

