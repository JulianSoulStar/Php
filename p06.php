<!--La instrucción echo es una de las más utilizadas en PHP,
    ya que nos permite visualizar en pantalla lo que escribamos detrás de esta palabra clave.
    Si lo que queremos visualizar es un texto, debemos escribirlo entre comillas.
    de la siguiente manera:-->
<?php
echo "Hola mundo";
print "<p>Hola mundo</p>";
//La instrucción print es en todo igual a la instruccion echo
//pudiendo usar una u otra indistintamente para obtener el mismo resultado
