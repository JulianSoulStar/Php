<?php

//json_encode — Retorna la representación JSON del valor dado
//string json_encode ( mixed $value [, int $options = 0 [, int $depth = 512 ]] )
//        Devuelve un string con la representación JSON de value.
//Ejemplo #1 Un ejemplo de json_encode()
$arr = array('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5);

echo json_encode($arr);

//El resultado del ejemplo sería:
//
//{"a":1,"b":2,"c":3,"d":4,"e":5}
