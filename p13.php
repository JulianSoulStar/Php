<?php

//una matriz tridimensional es una matriz de matrices de matrices :
$datos = array(
    array(array(0, 0, 0),
        array(0, 0, 1),
        array(0, 0, 2)
    ),
    array(array(0, 1, 0),
        array(0, 1, 1),
        array(0, 1, 2)
    ),
    array(array(0, 2, 0),
        array(0, 2, 1),
        array(0, 2, 2)
    )
);
//Como vemos, en la matriz del ejemplo hay una profundidad de 3 arrays, es decir,
//el primero contiene un segundo y el segundo de 3 terceros.



