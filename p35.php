<?php

//¿Qué son los espacios de nombres?
// En su definición más aceptada, los espacios de nombres son una manera de encapsular elementos.
//  Se pueden ver como un concepto abstracto en muchos aspectos. Por ejemplo,
// en cualquier sistema operativo, los directorios sirven para agrupar ficheros relacionados,
// actuando así como espacios de nombres para los ficheros que contienen. Como ejemplo,
// el fichero foo.txt puede existir en los directorios /home/greg y /home/otro,
// pero no pueden coexistir dos copias de foo.txt en el mismo directorio. Además,
// para acceder al fichero foo.txt fuera del directorio /home/greg, se debe anteponer el nombre del
// directorio al nombre del fichero, empleando el separador de directorios para así obtener
// /home/greg/foo.txt. Este mismo principio se extiende a los espacios de nombres en el mundo de la
// programación.
//
//ejemplo de sintaxis de espacios de nombres

namespace mi\nombre; // véase la sección "Definir espacios de nombres"

class MiClase {

}

function mifunción() {

}

const MICONSTANTE = 1;

$a = new MiClase;
$c = new \mi\nombre\MiClase; // véase la sección "Espacio global"

$a = strlen('hola'); // véase la sección "Utilizar espacios de nombres: una
// alternativa a funciones/constantes globales"

$d = namespace\MICONSTANTE; // véase la sección "El operador namespace y
// la constante __NAMESPACE__"
$d = __NAMESPACE__ . '\MICONSTANTE';
echo constant($d); // véase la sección "Espacios de nombres y características dinámicas del lenguaje"
