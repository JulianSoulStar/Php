<?php

// matriz multidimencional
// Una matriz multidimensional es una matriz que contiene una o más matrices.

//PHP entiende matrices multidimensionales que son dos, tres, cuatro, cinco, o más niveles de profundidad.
//Sin embargo, las matrices más de tres niveles de profundidad son difíciles de manejar
// para la mayoría de la gente.
// 
 //La dimensión de una matriz indica el número de índices que necesita para seleccionar un elemento.

//Para una matriz de dos dimensiones que necesita dos índices para seleccionar un elemento
//Para una matriz tridimensional se necesitan tres índices para seleccionar un elemento